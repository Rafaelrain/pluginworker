package br.com.infernalia.pluginworker;

import br.com.infernalia.pluginworker.command.PWCommand;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

public final class PluginWorker extends JavaPlugin {

    @Getter
    private static PluginWorker instance;

    @Override
    public void onLoad() {
        instance = this;
    }

    @Override
    public void onEnable() {
        getCommand("pluginworker").setExecutor(new PWCommand());
    }
}
