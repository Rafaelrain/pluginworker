package br.com.infernalia.pluginworker.testplugin;

import org.bukkit.plugin.java.JavaPlugin;

public final class TestPlugin extends JavaPlugin {

    @Override
    public void onLoad() {
        getLogger().info("onLoad...");
    }

    @Override
    public void onEnable() {
        getLogger().info("onEnable...");

    }

    @Override
    public void onDisable() {
        getLogger().info("onDisable...");
    }
}
