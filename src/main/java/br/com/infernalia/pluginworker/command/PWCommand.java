package br.com.infernalia.pluginworker.command;

import br.com.infernalia.pluginworker.util.PluginUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PWCommand implements CommandExecutor {

    private static <R> Predicate<R> not(Predicate<R> predicate) {
        return predicate.negate();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        // Inicio do Comando

        PluginUtil pluginUtil = new PluginUtil();

        if (args.length == 0) {
            getHelp(label).forEach(sender::sendMessage);
            return true;
        }

        // LIST
        if (args[0].equalsIgnoreCase("list")) {
            Plugin[] plugins = Bukkit.getPluginManager().getPlugins();
            return message(sender,
                    "§aPlugins ativados: §f" + Arrays.stream(plugins).filter(Plugin::isEnabled)
                            .map(Plugin::getName).collect(Collectors.joining(", ")
                            ),
                    "§cPlugins desativados: §f" + ifEmpty(
                            Arrays.stream(plugins).filter(not(Plugin::isEnabled))
                                    .map(Plugin::getName).collect(Collectors.joining(", ")
                            ), "§7Nenhum"
                    )
            );
        }

        // ENABLE
        if (args[0].equalsIgnoreCase("enable")) {
            if (args.length < 2)
                return message(sender,
                        " §a§l! §fUtilize §7/" + label + " enable <Plugin>");

            String name = getFrom(1, args);
            return message(sender,
                    pluginUtil.enablePlugin(name));
        }

        // DISABLE
        if (args[0].equalsIgnoreCase("disable")) {
            if (args.length < 2)
                return message(sender,
                        " §a§l! §fUtilize §7/" + label + " disable <Plugin>");

            String name = getFrom(1, args);
            Optional<Plugin> optional = pluginUtil.getByName(name);

            if (!optional.isPresent())
                return message(sender,
                        " §a§l! §fNenhum plugin com este nome foi encontrado: §7" + name);

            Plugin plugin = optional.get();
            return message(sender,
                    pluginUtil.disablePlugin(plugin));
        }

        if (args[0].equalsIgnoreCase("reload")) {
            if (args.length < 2)
                return message(sender,
                        " §a§l! §fUtilize §7/" + label + " reload <Plugin>");

            String name = getFrom(1, args);
            Optional<Plugin> optional = pluginUtil.getByName(name);

            if (!optional.isPresent())
                return message(sender,
                        " §a§l! §fNenhum plugin com este nome foi encontrado: §7" + name);

            Plugin plugin = optional.get();
            return message(sender,
                    pluginUtil.reloadPlugin(plugin));
        }
        getHelp(label).forEach(sender::sendMessage);
        return false;
    }

    private List<String> getHelp(String label) {
        return Arrays.asList(
                "- §e§lPLUGIN WORKER",
                "",
                "§f /" + label + "§e Enable §a<plugin>",
                "§f /" + label + "§e Disable §a<plugin>",
                "§f /" + label + "§e Reload §a<plugin>",
                "§f /" + label + "§e List §a<plugin>",
                "",
                "§e§l======================"
        );
    }

    private String getFrom(int start, String[] args) {
        StringBuilder builder = new StringBuilder();
        for (int i = start; i < args.length; i++) {
            if (i > start)
                builder.append(" ");
            builder.append(args[i]);
        }
        return builder.toString();
    }

    private boolean message(CommandSender sender, String... texts) {
        sender.sendMessage(texts);
        return true;
    }

    private String ifEmpty(String tester, String string) {
        return tester.isEmpty() ? string : tester;
    }
}