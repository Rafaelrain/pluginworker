package br.com.infernalia.pluginworker.util;

import br.com.infernalia.pluginworker.PluginWorker;
import org.bukkit.Bukkit;
import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.InvalidPluginException;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import java.io.File;
import java.util.Arrays;
import java.util.Optional;
import java.util.logging.Logger;

public class PluginUtil {

    private PluginManager pluginManager = Bukkit.getPluginManager();
    private File pluginsFolder = new File("plugins");

    public Optional<Plugin> getByName(String name) {
        return Arrays.stream(pluginManager.getPlugins())
                .filter(it -> it.getName().equalsIgnoreCase(name))
                .findFirst();
    }

    public String disablePlugin(Plugin plugin) {
        if (!plugin.isEnabled())
            return message("Este plugin já está desativado.");
        consoleSend(
                "",
                " Desativando o plugin: " + plugin.getName(),
                "");
        try {
            pluginManager.disablePlugin(plugin);
            return message("Plugin desativado com sucesso!");
        } catch (Exception e) {
            e.printStackTrace();
            return message("Algum erro foi ocorrido. Dê uma olhada no console");
        }
    }

    public String enablePlugin(String name) {
        Plugin plugin;

        // Checar se tem algum plugin desativado com esse nome
        plugin = pluginManager.getPlugin(name);
        // Caso não, tentar encontrar algum arquivo
        if (plugin == null)
            return loadFromFile(name);

        if (pluginManager.isPluginEnabled(plugin))
            return message("Este plugin já está iniciado.");
        //OnLoad
        plugin.onLoad();
        //onEnable
        pluginManager.enablePlugin(plugin);
        return message("§ePlugin iniciado com sucesso.");
    }

    private String loadFromFile(String name) {
        File file = new File(
                pluginsFolder,
                name.endsWith(".jar") ? name : name + ".jar"
        );
        if (!file.exists())
            return message("Não foi encontrado nenhum arquivo com este nome: §7" + name);
        try {
            // Load
            Plugin plugin = pluginManager.loadPlugin(file);
            // onLoad
            if (plugin != null) {
                plugin.onLoad();
            }
            // onEnable
            pluginManager.enablePlugin(plugin);
            return message("Plugin carregado com sucesso.");
        } catch (InvalidPluginException e) {
            e.printStackTrace();
            return message("Não foi encontrado nenhum plugin no arquivo: §7" + file.getName());
        } catch (InvalidDescriptionException e) {
            e.printStackTrace();
            return message("Erro ao carregar a descrição do arquivo: §7" + file.getName());
        }
    }

    public String reloadPlugin(Plugin plugin) {
        try {
            disablePlugin(plugin);
            enablePlugin(plugin.getName());
            return message("Plugin Recarregado com sucesso!");
        } catch (Exception e) {
            e.printStackTrace();
            return message("Ocorreu algum erro. Dê uma olhada no console.");
        }
    }

    private String message(String text) {
        return "§a§l[PluginWorker] §f§l» §f" + text;
    }

    private void consoleSend(String... text) {
        Logger logger = PluginWorker.getInstance().getLogger();
        for (String s : text) {
            logger.info(s);
        }
    }
}
